$(document).ready(function() {
   
    $('.artist').click(function() {
	    $("#artistRow>button.active").removeClass("active");
	    $("#albums").empty();
	    $("#songs").empty();
	    $(this).addClass('active');
	       var id = $(this).data('artistid');
		   $.get(baseURL + 'api/artist/' + id, function(data) {
			  	$.each(data, function(i, item) {
					$("#albums").append('<button type="button" onclick="album(this);" class="list-group-item album" data-albumid="' + data[i].id + '">' + data[i].name + '<span class="badge">' + data[i].songCount + '</span></button>');
				});		
		});
	});
});

function album(event){
	$("#albums>button.active").removeClass("active");
	$("#songs").empty();
	$(event).addClass('active');
	var id = $(event).data('albumid');
	$.get(baseURL + 'api/album/' + id, function(data) {
	 	$.each(data, function(i, item) {
			$("#songs").append('<button type="button" class="list-group-item song" onclick="song(this);" data-songid="' + data[i].id + '">' + data[i].name + '<span class="badge">' + data[i].playCount + '</span></button>');
		});		
	});
}

function song(event){
	$("#songs>button.active").removeClass("active");
	var id = $(event).data('songid');
	$(event).addClass('active');
	$.get(baseURL + 'api/song/' + id + '/link', function(data) {
		$("#url").html(data);
		$("#url").attr('href', data);
	});
}
