<?php

/**
* 
*/
class Subsonic
{
	
	protected $APIstring;
	protected $pars;
	protected $artistArray;

	public function Subsonic($user, $password, $ver, $host, $appName){
		$this->APIstring = $host . '/';

		$this->pars = '?u=' . $user . '&p=' . $password . '&v=' . $ver . '&c=' . $appName . '&f=json';  
	}

	public function testConnection(){
		$res = file_get_contents($this->APIstring . 'rest/ping.view' . $this->pars);
		$subResJSON = json_decode($res);
		if ($subResJSON->{'subsonic-response'}->status == 'ok'){
			return 1;
		}	
	}

	public function getArtists(){
		$this->artistArray = array();

		$res = file_get_contents($this->APIstring . 'rest/getArtists.view' . $this->pars);
		$subResJSON = json_decode($res);

		foreach ($subResJSON->{'subsonic-response'}->artists->index as $index) {
			foreach ($index->artist as $artist) {
				$this->artistArray[] = array(
					'id' => $artist->id, 
					'name' => $artist->name, 
					//'coverArt' => $artist->coverArt, 
					'albumCount' => $artist->albumCount
					);	
			}	
		}

		return $this->artistArray;
	}


	public function getArtist($id){
		$this->artistArray = array();

		$res = file_get_contents($this->APIstring . 'rest/getArtist.view' . $this->pars . '&id=' . $id);
		$subResJSON = json_decode($res);

		foreach ($subResJSON->{'subsonic-response'}->artist->album as $album) {
			$albumArray[] = array(
					'id' => $album->id, 
					'name' => $album->name, 
					//'coverArt' => $album->coverArt, 
					'songCount' => $album->songCount
					);
		}
		return $albumArray;	
	}

	public function getAlbum($id){
		$this->ablumArray = array();

		$res = file_get_contents($this->APIstring . 'rest/getAlbum.view' . $this->pars . '&id=' . $id);
		$subResJSON = json_decode($res);
		
		foreach ($subResJSON->{'subsonic-response'}->album->song as $song) {
			$songArray[] = array(
					'id' => $song->id, 
					'name' => $song->title, 
					'playCount' => $song->playCount,
					'contentType' => $song->contentType,
					'suffix' => $song->suffix,
					'duration' => $song->duration,
					'bitRate' => $song->bitRate,
					'path' => $song->path,
					);
		}
		
		return $songArray;	
	}


	public function getSongStream($id){

		$res = file_get_contents($this->APIstring . 'rest/stream.view' . $this->pars . '&id=' . $id . '&maxBitRate=320&format=mp3');
		return $res;	
	}

	public function getSongLink($id){
		return $this->APIstring . 'rest/stream.view' . $this->pars . '&id=' . $id . '&maxBitRate=320&format=mp3';	
	}
}


?>