<?php


// Define app routes
$app->get('/', function () use ($app) {
	$s = new Subsonic(USERNAME, PASSWORD, APIV, HOST, APPNAME);
	$artists = $s->getArtists();

	$app->render('home.php', array('artists' => $artists, 'baseURL' => BASEURL));

})->name('home');

$app->get('/api/artist/:id', function ($id) use ($app) {
	$app->response->headers->set('Content-Type', 'application/json');

	$s = new Subsonic(USERNAME, PASSWORD, APIV, HOST, APPNAME);
	$artist = $s->getArtist($id);
	echo json_encode($artist);
});

$app->get('/api/album/:id', function ($id) use ($app) {
	$app->response->headers->set('Content-Type', 'application/json');

	$s = new Subsonic(USERNAME, PASSWORD, APIV, HOST, APPNAME);
	$album = $s->getAlbum($id);
	echo json_encode($album);
});

$app->get('/api/song/:id/stream', function ($id) use ($app) {
	$app->response->headers->set('Content-Type', 'audio/mp3');

	$s = new Subsonic(USERNAME, PASSWORD, APIV, HOST, APPNAME);
	$stream = $s->getSongStream($id);
	echo $stream;
});

$app->get('/api/song/:id/link', function ($id) use ($app) {
	
	$s = new Subsonic(USERNAME, PASSWORD, APIV, HOST, APPNAME);
	echo $s->getSongLink($id);
	
});