<?php

require 'settings.php';

//session_start();

date_default_timezone_set("UTC");

require '../vendor/autoload.php';


$app = new \Slim\Slim(array(
	 'debug' => DEBUG,
     'templates.path' => '../app/views'
	));

//include all routes 
require 'routes.php';

//include models
require 'models.php';
