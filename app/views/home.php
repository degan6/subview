<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Subsonic Viewer</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
    </style>


</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Start Bootstrap</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <h3>URL</h3>
                <div class="text-center">
                    <div class="well">
                        <h5><a id="url" href="" target="_blank" style="color:black;"></a></h5>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-4"  style="overflow: hidden">
                <h2>Artists</h2>
                <div class="list-group" id="artistRow" style="overflow-y: scroll; height:630px;">
                    <?php
                        foreach ($artists as $key) {
                            echo '<button type="button" class="list-group-item artist" data-artistID="' . $key['id'] . '">' . $key['name'] . '<span class="badge">' . $key['albumCount'] . '</span></button>';
                        }
                    ?>
                </div>
            </div>
            <div class="col-lg-4" style="overflow: hidden">
                <h2>Albums</h2>
                <div class="list-group" id="albums" style="overflow-y: scroll; height:630px;">
                  
                </div>
            </div>
            <div class="col-lg-4" style="overflow: hidden">
                <h2>Songs</h2>
                <div class="list-group" id="songs" style="overflow-y: scroll; height:630px;">
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
    <script type="text/javascript">
        var baseURL = "<?php echo $baseURL; ?>";
    </script>

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sub.js"></script>

</body>

</html>